import LoginPage from '../support/page-objects/loginPage';
import EmployeePage from '../support/page-objects/employeePage';

describe('Employee Test', () => {
  beforeEach(() => {
    LoginPage.visit();
    LoginPage.login('admin', 'admin123');
  });

  it('should add an employee', () => {
    EmployeePage.visit();
    const firstName = 'Lusy';
    const lastName = 'M';
    const employeeId = 'EMP127';

    EmployeePage.addEmployee(firstName, lastName, employeeId);

    EmployeePage.assertEmployeeExists(firstName, lastName);
  });

  it('should search for an employee', () => {
    EmployeePage.visit();
    const firstName = 'Lusy';
    const lastName = 'M';

    EmployeePage.searchEmployee(firstName, lastName);

    EmployeePage.assertEmployeeExists(firstName, lastName);
  });

  it('should delete an employee', () => {
    EmployeePage.visit();
    const firstName = 'Lusy';
    const lastName = 'M';

    EmployeePage.searchEmployee(firstName, lastName);
    EmployeePage.deleteEmployee(firstName, lastName);

    EmployeePage.assertEmployeeDoesNotExist(firstName, lastName);
  });
});
