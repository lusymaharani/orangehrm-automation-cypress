import LoginPage from '../support/page-objects/loginPage';

describe('OrangeHRM Login Test', () => {
  it('should log in with valid credentials', () => {
    LoginPage.visit();
    LoginPage.login('admin', 'admin123');
  });

  it('should display an error message for invalid credentials', () => {
    LoginPage.visit();
    LoginPage.login('invalidusername', 'invalidpassword');
    LoginPage.assertLoginErrorMessage('Invalid credentials');
  });
});
