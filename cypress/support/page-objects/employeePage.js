class EmployeePage {
    visit() {
      cy.visit('https://opensource-demo.orangehrmlive.com/web/index.php/pim/viewEmployeeList');
    }
  
    addEmployee(firstName, lastName, employeeId) {
      cy.get('.orangehrm-header-container > .oxd-button').click();
      cy.get('.--name-grouped-field > :nth-child(1) > :nth-child(2) > .oxd-input').type(firstName);
      cy.get(':nth-child(3) > :nth-child(2) > .oxd-input').type(lastName);
      cy.get('.oxd-grid-item > .oxd-input-group > :nth-child(2) > .oxd-input').type(employeeId);
      cy.get('.oxd-button--secondary').click();
    }
   
    searchEmployee(firstName, lastName) {
      cy.wait(5000)
      cy.get(':nth-child(1) > .oxd-input-group > :nth-child(2) > .oxd-autocomplete-wrapper > .oxd-autocomplete-text-input > input').type(`${firstName} ${lastName}`);
      cy.get('.oxd-form-actions > .oxd-button--secondary').click();
    }

    deleteEmployee(firstName, lastName) {
      cy.wait(5000)
      cy.contains(`${firstName} ${lastName}`).find('.oxd-icon.bi-trash').click({ multiple: true, force: true });
      cy.get('.oxd-button--label-danger').click();
    }
  
    assertEmployeeExists(firstName, lastName) {
      cy.contains(`${firstName} ${lastName}`).should('exist');
    }
  
    assertEmployeeDoesNotExist(firstName, lastName) {
      cy.contains(`${firstName} ${lastName}`).should('not.exist');
    }
  }
  
  export default new EmployeePage();
  