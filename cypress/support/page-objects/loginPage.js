class LoginPage {
    visit() {
      cy.visit('https://opensource-demo.orangehrmlive.com/web/index.php/auth/login');
    }
  
    fillUsername(username) {
      cy.get(':nth-child(2) > .oxd-input-group > :nth-child(2) > .oxd-input').type(username);
    }
  
    fillPassword(password) {
      cy.get(':nth-child(3) > .oxd-input-group > :nth-child(2) > .oxd-input').type(password);
    }
  
    clickLoginButton() {
      cy.get('.oxd-button').click();
    }
  
    login(username, password) {
      this.fillUsername(username);
      this.fillPassword(password);
      this.clickLoginButton();
    }
  
    assertLoginErrorMessage(message) {
      cy.get('.oxd-alert-content').should('have.text', message);
    }
  }
  
  export default new LoginPage();
  