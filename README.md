# Guide

This is a quick guide to running this automation test. Make sure you have Node.js and npm installed on your PC.

## Step 1: Install Dependencies

Before you can run the tests, you need to install all the project dependencies. Run the following command in your terminal:

```bash
npm install
